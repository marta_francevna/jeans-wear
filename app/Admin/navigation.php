<?php

use SleepingOwl\Admin\Navigation\Page;

AdminSection::addMenuPage(\App\Models\News::class)
	->setIcon('fa fa-newspaper-o');
AdminSection::addMenuPage(\App\Models\Catalog::class)
	->setIcon('fa fa-shopping-bag');
AdminSection::addMenuPage(\App\Models\Colors::class)
	->setIcon('fa fa-paint-brush');
AdminSection::addMenuPage(\App\Models\Size::class)
	->setIcon('fa fa-tag');
//AdminSection::addMenuPage(\App\Models\Comments::class)
//	->setIcon('fa fa-comment');
AdminSection::addMenuPage(\App\Models\Feedback::class)
	->setIcon('	fa fa-envelope-o');
