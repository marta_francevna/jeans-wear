<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\News
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[]
 *                $notifications
 * @mixin \Eloquent
 */
class News extends Model
{
	public $imgPath = '/img/news/';

	protected $fillable = [
		'slug', 'image', 'title', 'text',
	];

	public function getImageAttribute($value)
	{
		$str = strpos($value, "news");
		$value = $str ? substr($value, $str+5, strlen($value)):$value;
		return $this->imgPath . $value;
	}

	public function setImageAttribute($value)
	{
		$str  = strpos($value, "news");
		$value = $str ? substr($value, $str+5, strlen($value)):$value;
		$this->attributes['image'] = $value;
	}

	protected static function boot()
	{
		parent:: boot();

		static::creating(function (Model $model) {
			$model->slug = str_slug($model->title, '-');

			return $model;
		});

		static::updating(function (Model $model) {
			$model->slug = str_slug($model->title, '-');

			return $model;
		});
	}

}
