<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Category
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class Category extends Model
{
	protected $table = 'category';

	public function catalogs()
	{
		return $this->hasMany(Catalog::class, 'category_id');
	}

}
