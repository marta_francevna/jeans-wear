<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\Comments
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */

class Comments extends Model
{
	protected $fillable = ['message', 'catalog_id'];

	public function catalog()
	{
		return $this->belongsTo(Catalog::class);
	}
}
