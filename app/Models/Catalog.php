<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\Catalog
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */

class Catalog extends Model
{
	protected $table = 'catalog';

	protected $fillable = [
		'category_id', 'image', 'composition', 'vendor_code',
	];

	public $imgPath = '/img/catalog/';

	public function colors()
	{
		return $this->belongsToMany(Colors::class, 'catalog_colors');
	}

	public function size()
	{
		return $this->belongsToMany(Size::class, 'catalog_size');
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function getImageAttribute($value)
	{
		$str = strpos($value, "catalog");
		$value = $str ? substr($value, $str+8, strlen($value)):$value;
		return $this->imgPath . $value;
	}

	public function setImageAttribute($value)
	{
		$str  = strpos($value, "catalog");
		$value = $str ? substr($value, $str+8, strlen($value)):$value;
		$this->attributes['image'] = $value;
	}



}
