<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\Colors
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class Colors extends Model
{

	protected $fillable = [
		'name'
	];

	public function catalogs()
	{
		return $this->belongsToMany(Catalog::class, 'catalog_colors');
	}

}
