<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\Size
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class Size extends Model
{
	protected $table = 'size';

	protected $fillable = [
		'name'
	];

	public function catalogs()
	{
		return $this->belongsToMany(Catalog::class, 'catalog_size');
	}

}
