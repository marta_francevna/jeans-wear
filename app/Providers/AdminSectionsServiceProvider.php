<?php

namespace App\Providers;

use App\Models\Catalog;
use App\Models\Colors;
use App\Models\Comments;
use App\Models\Feedback;
use App\Models\News;
use App\Models\Size;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        Catalog::class => 'App\Http\Sections\Catalog',
        News::class => 'App\Http\Sections\News',
        Colors::class => 'App\Http\Sections\Colors',
        Size::class => 'App\Http\Sections\Size',
        Comments::class => 'App\Http\Sections\Comments',
        Feedback::class => 'App\Http\Sections\Feedback',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
	    parent::boot($admin);
	    $this->app->call([$this, 'registerMediaPackages']);

    }

	/**
	 * @param MetaInterface $meta
	 */
	public function registerMediaPackages(MetaInterface $meta)
	{
		$packages = $meta->assets()->packageManager();

		require base_path('/app/Admin/assets.php');
	}
}
