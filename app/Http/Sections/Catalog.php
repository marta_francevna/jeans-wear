<?php

namespace App\Http\Sections;

use App\Models\Category;
use KodiCMS\Assets\Meta;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

/**
 * Class Catalog
 *
 * @property \App\Models\Catalog $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Catalog extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Каталог';

    /**
     * @var string
     */
    protected $alias = 'catalogs';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
	    return AdminDisplay::table()->with('category')->setApply(function($query) {
		    $query->orderBy('created_at', 'desc');
	    })->setColumns([
		    AdminColumn::text('id', '#')->setWidth('50px'),
		    $photo = AdminColumn::image('image', 'Фото')
			    ->setHtmlAttribute('class', 'hidden-sm hidden-xs foobar')
			    ->setWidth('200px'),

		    AdminColumn::link('category.name', 'Заголовок')->setWidth('200px'),
	        AdminColumn::text('vendor_code', 'Артикул'),
            AdminColumn::text('price', 'Цена BYN'),
	    ])->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {

	    $form = AdminForm::form()->setElements([

		    AdminFormElement::select('category_id')->setLabel('Категория')
			    ->setModelForOptions(Category::class)
			    ->setHtmlAttribute('placeholder', 'Выберите категорию')
			    ->setDisplay('name')
			    ->required()
		        ->setHtmlAttribute('id', 'category_id'),
		    AdminFormElement::multiselect('colors', 'Цвета', \App\Models\Colors::class)
			    ->setDisplay('name')
			    ->required(),
		    AdminFormElement::multiselect('size', 'Размеры', \App\Models\Size::class)
			    ->setDisplay('name')
			    ->required()
			    ->setHtmlAttribute('id', 'size'),
		    AdminFormElement::textarea('composition', 'Состав')->required()->setRows(3),
		    AdminFormElement::text('vendor_code', 'Артикул')->required(),
            AdminFormElement::number( 'price', 'Цена')->required(),
		    $field = AdminFormElement::image('image', "Изображение")->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
			    return 'img/catalog';
		    })->required(),


	    ]);
	    return $form;

    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
