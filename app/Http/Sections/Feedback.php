<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

/**
 * Class Feedback
 *
 * @property \App\Models\Feedback $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Feedback extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Обратная связь';

    /**
     * @var string
     */
    protected $alias = 'feedback';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
	    return AdminDisplay::table()
		    ->setHtmlAttribute('class', 'table-primary')
		    ->setColumns([
			    AdminColumn::text('name', 'Имя'),
			    AdminColumn::email('email', 'Email')->setWidth('150px'),
			    AdminColumn::text('message', 'Сообщение'),
		    ])->paginate(20);
    }

    /**


     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
