<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;


/**
 * Class News
 *
 * @property \App\Models\News $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class News extends Section
{
	/**
	 * @var \App\Models\News
	 */
	protected $model = '\App\Models\News';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Новости';

    /**
     * @var string
     */
    protected $alias = 'news';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
	    return AdminDisplay::table()->setApply(function($query) {
		    $query->orderBy('created_at', 'desc');
	    })->setColumns([
			    AdminColumn::text('id', '#')->setWidth('30px'),
			    $photo = AdminColumn::image('image', 'Фото')
				    ->setHtmlAttribute('class', 'hidden-sm hidden-xs foobar')
				    ->setWidth('100px'),

			    AdminColumn::link('title', 'Заголовок')->setWidth('200px')
		    ])->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
	    $form = AdminForm::form()->setElements([
		    AdminFormElement::text('title', 'Заголовок')->required(),
		    AdminFormElement::ckeditor('text', 'Текст')->required(),
		    $field = AdminFormElement::image('image', "Изображение")->setUploadPath(function(\Illuminate\Http\UploadedFile $file) {
			    return 'img/news';
		    })->required()

	    ]);
	    return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
	//заголовок для создания записи
	public function getCreateTitle()
	{
		return 'Создание новости';
	}
}
