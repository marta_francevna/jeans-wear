<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Category;
use App\Models\Colors;
use App\Models\Comments;
use App\Models\Size;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
	//все каталоги
	public function index()
	{
		//все каталоги, цвета,размеры и каталоги
		$catalogs   = Catalog::paginate(9);
		$colors     = Colors::all();
		$sizes      = Size::all();
		$categories = Category::all();
		//передать на страницу каталога
		return view('catalog.index', compact('catalogs', 'colors', 'sizes', 'categories'));
	}

	//один каталог
	public function show($sex, $category, $id)
	{
		//Найти категории где пол равен тому, которы мы передали
		$Sex = Category::where('sex', $sex)->first();
		//найти категорию где слаг как слаг категории, который мы передали
		$Category = Category::where('slug', $category)->first();
		//Если такие нашли, то продолжаем работу
		if($Sex && $Category){
			//найти каталоги и каменты
			$catalog  = Catalog::with('category', 'colors', 'size')->find($id);
			$comments = Comments::where('catalog_id', $id)->get();
			//если каталоги есть, то передаем всё на страницу каталлога
			return $catalog
				?  view('catalog.show', compact('catalog', 'comments'))
				: abort(404);
			//иначе страница 404
		}

		return abort(404);

	}

	//по полу
	public function sex($sex)
	{
		//найти все цвета, размеры, категории
		$colors     = Colors::all();
		$sizes      = Size::all();
		$categories = Category::all();

		//найти каталоги у которых категория равна полу которы мы передали в параметры
		$catalogs = Catalog::whereHas('category', function ($query) use ($sex) {
			$query->where('sex', $sex);
		})->paginate(9);
		//и постраничная навигация

		//передать всё на страницу
		return view('catalog.sex', compact('catalogs', 'colors', 'sizes', 'categories', 'sex'));
	}

	//по категории
	public function category($sex, $categoryUrl)
	{
		//всё аналогично
		$colors     = Colors::all();
		$sizes      = Size::all();
		$categories = Category::all();
		$category   = Category::where('slug', $categoryUrl)->where('sex', $sex)->first();

		if ($category) {
			$catalogs = Catalog::whereHas('category', function ($query) use ($category) {
				$query->where('id', $category->id);
			})->paginate(9);
		}

		return $category
			? view('catalog.category', compact('catalogs', 'colors', 'sizes', 'categories', 'category'))
			: abort(404);

	}

	//по поиску
	public function search(Request $request)
	{
		//берем поля категория, цвет и размер из параметров поиска
		$category = $request->input('category');
		$color    = $request->input('color');
		$size     = $request->input('size');

		//находим каталоги с такими условиями
		$catalogs = self::findCatalogs($category, $color, $size);

		$colors     = Colors::all();
		$sizes      = Size::all();
		$categories = Category::all();

		$title = ['category' => Category::find($category), 'color' => Colors::find($color), 'size' => Size::find($size)];

		return view('catalog.search', compact('catalogs', 'colors', 'sizes', 'categories', 'title'));
	}

	//добавление камента
	public function comment(Request $request, $id)
	{
		//берем всё, что пришло из формы
		$data = $request->all();
		$data['catalog_id'] = $id;
		//создаем камент
		Comments::create($data);
		//редиректим обратно
		return redirect()->back();
	}

	public static function findCatalogs($category, $color, $size)
	{
		//создаем запрос
		$catalogs = Catalog::query();

		if ( $category !== '' ) {
			//где категория равна нашему параметру итд
			$catalogs->where('category_id', $category);
		}

		if ( $color !== '' ) {
			$catalogs->whereHas('colors', function ($query) use ($color) {
				$query->where('id', $color);
			});
		}

		if ( $size !== '' ) {
			$catalogs->whereHas('size', function ($query) use ($size) {
				$query->where('id', $size);
			});
		}
		// и добавляем постраничную навигацию
		return $catalogs->paginate(9);
	}
}
