<?php

namespace App\Http\Controllers;

use App\Models\Catalog;
use App\Models\Feedback;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

class HomeController extends Controller
{


	public function index()
	{

		//выбрать из таблицы новости последние 10 новостей
		$news = News::latest('created_at')->limit(10)->get();
		//выбрать из таблицы каталог последние 10 каталогов
		$latestCatalogs = Catalog::with('category')->latest('created_at')->limit(10)->get();
		//найти все каталоги
		$catalog = Catalog::with('category')->get()->toArray();

		//передать на страницу
		return view('home.index', compact('news', 'catalog', 'latestCatalogs'));
	}

	public function feedback(Request $request)
	{
		//берем все данные из формы
		$data = $request->all();
		//создаем обратную связь
		Feedback::create($data);

		$message = $request->input('message');

		//отправляем сообщение
		\Mail::raw($message, function (Message $mail) {
			$mail->subject('Обратная связь JeansWear');
			$mail->from('5665078@gmail.com', 'Обратная связь JeansWear');//почта с какой отправлять
			$mail->to('5665078@gmail.com');//почта на которую отправлять
		});

		/*
		 * Настройка env
		 * MAIL_DRIVER=smtp
		 * MAIL_HOST=smtp.gmail.com
		 * MAIL_PORT=587
		 * MAIL_USERNAME=5665078@gmail.com
		 * MAIL_PASSWORD=27112711
		 *  MAIL_ENCRYPTION=tls
		 */

		return redirect()->back();
	}

	public function contact(){

		//открыть страницу контактов
		return view('home.contact');

	}

	public function contactFeedback(Request $request){

		$data = $request->all();
		Feedback::create($data);

		$message = $request->input('message');
		$name = $request->input('name');
		$email = $request->input('email');

		\Mail::raw($message . ' Email: ' . $email, function (Message $mail) use ($name, $email) {
			$mail->subject('Обратная связь JeansWear | сообщение от ' . $name);
			$mail->from('5665078@gmail.com', 'Обратная связь JeansWear | сообщение от ' . $name);//почта с какой отправлять
			$mail->to('5665078@gmail.com');//почта на которую отправлять
		});

		return redirect()->back();
	}

	public function gallery(){

		//найти каталоги и их категории и отсортировать по дате
		$catalogs = Catalog::with('category')->latest('created_at')->get();

		return view('home.gallery', compact('catalogs'));
	}

}
