<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

	public function index() {

		$news = News::latest('created_at')->paginate(3);
		return view( 'news.index', compact('news'));
	}

	public function show($url) {

		//найти новости
		$news = News::where('slug', $url)->first();

		if ($news) {
			//найти id след новости
			$idNext     = News::where('id', '>', $news->id)->min('id');
			//найти id прошлой новости
			$idPrevious = News::where('id', '<', $news->id)->max('id');
			if ( $idNext ) {
				$next = News::find($idNext);
			}
			if ( $idPrevious ) {
				$previous = News::find($idPrevious);
			}
		}

		return $news
		? view('news.show', compact('news', 'previous', 'next'))
            : abort(404);
	}

}
