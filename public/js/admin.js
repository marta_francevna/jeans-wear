// Avoid `console` errors in browsers that lack a console.
(function() {

	$(document).ready(function(){
			$('#category_id').change(
				function(){
					var category = $('#category_id').val();
					if (category === '9' || category === '10'){
						$(".form-element-multiselect").eq(1).hide();
						$("#size").val('');
						$(".multiselect__tags-wrap").eq(2).empty();
					}else {
						$(".form-element-multiselect").eq(1).show();
					}
				});
		}
	);

}());