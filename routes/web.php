<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
		'as'   => 'index',
		'uses' => 'HomeController@index',
	]
);
//
//Route::post('login', [
//     'as'   => 'login',
//     'uses' => 'Auth\LoginController@login'
//]);
//Route::post('logout', [
//	'as'   => 'logout',
//	'uses' => 'Auth\LoginController@logout'
//]);
Auth::routes();


Route::post('/feedback', [
		'as'   => 'feedback',
		'uses' => 'HomeController@feedback',
	]
);

Route::get('/contact', [
		'as'   => 'contact',
		'uses' => 'HomeController@contact',
	]
);

Route::post('/contact/feedback', [
		'as'   => 'contact.feedback',
		'uses' => 'HomeController@contactFeedback',
	]
);

Route::get('/gallery', [
		'as'   => 'gallery',
		'uses' => 'HomeController@gallery',
	]
);




Route::group(['prefix' => 'news'], function () {

	Route::get('', [
		'as'   => 'news',
		'uses' => 'NewsController@index',
	]);

	Route::get('{slug}', [
		'as'   => 'post',
		'uses' => 'NewsController@show',
	])->where('slug', '[A-Za-z0-9-_]+');
});

Route::group(['prefix' => 'catalog'], function () {

	Route::get('', [
		'as'   => 'catalogs',
		'uses' => 'CatalogController@index',
	]);

	Route::get('search', [
		'as'   => 'catalogs.search',
		'uses' => 'CatalogController@search',
	]);

	Route::get('{sex}', [
		'as'   => 'catalog.sex',
		'uses' => 'CatalogController@sex',
	]);

	Route::get('{sex}/{slug}', [
		'as'   => 'category',
		'uses' => 'CatalogController@category',
	])->where('slug', '[A-Za-z0-9-_]+');


	Route::get('{sex}/{slug}/{id}', [
		'as'   => 'catalog',
		'uses' => 'CatalogController@show',

	])->where('slug', '[A-Za-z0-9-_]+');

	Route::post('comment/{id}', [
		'as'   => 'catalog.comment',
		'uses' => 'CatalogController@comment',
	]);

});

