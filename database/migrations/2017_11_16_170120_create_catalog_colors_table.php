<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('catalog_colors', function (Blueprint $table) {
		    $table->unsignedInteger('catalog_id');
		    $table->unsignedInteger('colors_id');
		    $table->timestamps();

		    $table->unique(['catalog_id', 'colors_id']);

		    $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
		    $table->foreign('colors_id')->references('id')->on('colors')->onDelete('cascade');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('catalog_colors');
    }
}
