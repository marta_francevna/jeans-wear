<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('comments', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('catalog_id');
		    $table->string('message');
		    $table->timestamps();

		    $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('comments');
    }
}
