<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('catalog_size', function (Blueprint $table) {
		    $table->unsignedInteger('catalog_id');
		    $table->unsignedInteger('size_id');
		    $table->timestamps();

		    $table->unique(['catalog_id', 'size_id']);

		    $table->foreign('catalog_id')->references('id')->on('catalog')->onDelete('cascade');
		    $table->foreign('size_id')->references('id')->on('size')->onDelete('cascade');

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('catalog_size');
    }
}
