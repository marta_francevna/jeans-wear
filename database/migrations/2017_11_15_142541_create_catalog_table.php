<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('catalog', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('category_id');
		    $table->string('image');
		    $table->string('composition');
		    $table->string('vendor_code');
            $table->double('price');
		    $table->timestamps();

		    $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('catalog');
    }
}
