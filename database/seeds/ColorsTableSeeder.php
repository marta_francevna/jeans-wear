<?php

use Illuminate\Database\Seeder;
use App\Models\Colors;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Colors::create([
		    'name' => 'Голубой',
	    ]);
	    Colors::create([
		    'name' => 'Чёрный',
	    ]);
	    Colors::create([
		    'name' => 'Синий',
	    ]);
	    Colors::create([
		    'name' => 'Светло-голубой',
	    ]);
	    Colors::create([
		    'name' => 'Белый',
	    ]);

    }
}
