<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Feedback;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $faker = Faker::create();

	    foreach ( range(1, 5) as $index => $value ) {

		    Feedback::create([
			    'message'  => $faker->text(150),
		    ]);
	    }
    }
}
