<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Catalog;
use App\Models\Category;

class CatalogTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		/*
		 * Мужская одежда
		 */
		foreach ( range(1, 20) as $index => $value ) {

			$categories = Category::where('sex', 'men')->pluck('id')->toArray();

			$catalog = Catalog::create([
				'category_id' => array_random($categories),
				'image'       => $faker->image(public_path('img/catalog'), 480, 606, 'fashion', false),
				'composition' => $faker->text(40),
				'vendor_code' => $faker->text(20),
                'price'       => random_int(1,200),
			]);
			$color   = random_int(1, 5);
			$size    = random_int(1, 6);
			$catalog->colors()->attach($color);
			$catalog->size()->attach($size);
		}
		/*
		* Женская одежда
		*/
		foreach ( range(1, 20) as $index => $value ) {

			$categories = Category::where('sex', 'woman')->pluck('id')->toArray();

			$catalog = Catalog::create([

				'category_id' => array_random($categories),
				'image'       => $faker->image(public_path('img/catalog'), 480, 606, 'fashion', false),
				'composition' => $faker->text(40),
				'vendor_code' => $faker->text(20),
                'price'       => random_int(1,200),
			]);

			$color = random_int(1, 5);
			$size  = random_int(1, 6);
			$catalog->colors()->attach($color);
			$catalog->size()->attach($size);
		}

		/*
		* Головные уборы
		*/
		foreach ( range(1, 10) as $index => $value ) {

			$categories = Category::where('sex', 'unisex')->pluck('id')->toArray();

			$catalog = Catalog::create([
				'category_id' => array_random($categories),
				'image'       => $faker->image(public_path('img/catalog'), 480, 606, 'fashion', false),
				'composition' => $faker->text(40),
				'vendor_code' => $faker->text(20),
                'price'       => random_int(1,200),
			]);

			$color = random_int(1, 5);
			$size  = random_int(1, 6);
			$catalog->colors()->attach($color);
			$catalog->size()->attach($size);
		}
	}
}
