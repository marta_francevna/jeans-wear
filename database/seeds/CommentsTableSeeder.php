<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Comments;
use App\Models\Catalog;

class CommentsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();
		foreach ( range(1, 10) as $index => $value ) {

			$catalogs = Catalog::all()->pluck('id')->toArray();

			Comments::create([
				'catalog_id' => array_random($catalogs),
				'message'    => $faker->text(100),
			]);
		}
	}
}
