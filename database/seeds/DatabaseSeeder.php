<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(UsersTableSeeder::class);
		$this->call(NewsTableSeeder::class);
		$this->call(ColorsTableSeeder::class);
		$this->call(SizeTableSeeder::class);
		$this->call(FeedbackTableSeeder::class);
		$this->call(CategoryTableSeeder::class);
		$this->call(CatalogTableSeeder::class);
		$this->call(CommentsTableSeeder::class);
	}
}
