<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Category::create([
			'name' => 'Мужская джинсовка',
			'slug' => 'jacket',
			'sex'  => 'men',
		]);
		Category::create([
			'name' => 'Женская джинсовка',
			'slug' => 'jacket',
			'sex'  => 'woman',
		]);
		Category::create([
			'name' => 'Мужские джинсы',
			'slug' => 'jeans',
			'sex'  => 'men',
		]);
		Category::create([
			'name' => 'Женские джинсы',
			'slug' => 'jeans',
			'sex'  => 'woman',
		]);
		Category::create([
			'name' => 'Мужские шорты',
			'slug' => 'shorts',
			'sex'  => 'men',
		]);
		Category::create([
			'name' => 'Женские шорты',
			'slug' => 'shorts',
			'sex'  => 'woman',
		]);
		Category::create([
			'name' => 'Костюм',
			'slug' => 'costume',
			'sex'  => 'men',
		]);
		Category::create([
			'name' => 'Комбенизон',
			'slug' => 'overalls',
			'sex'  => 'woman',
		]);
		Category::create([
			'name' => 'Кепка',
			'slug' => 'cap',
			'sex'  => 'unisex',
		]);
		Category::create([
			'name' => 'Панама',
			'slug' => 'panama',
			'sex'  => 'unisex',
		]);
	}
}
