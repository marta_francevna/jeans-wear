<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\News;

class NewsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create();

		foreach ( range(1, 20) as $index => $value ) {

			News::create([
				'slug'  => $faker->slug,
				'image' => $faker->image(public_path('img/news'), 870, 430, 'abstract', false),
				'title' => $faker->text(20),
				'text'  => $faker->text(500),
			]);
		}
	}
}
