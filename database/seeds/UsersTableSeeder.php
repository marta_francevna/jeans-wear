<?php

use Illuminate\Database\Seeder;
use \App\Models\Users;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    \App\Models\User::create( [
		    'email'      => 'admin@gmail.com',
		    'name'   => 'admin',
		    'password'   => bcrypt( 'admin' ),
	    ] );
    }
}
