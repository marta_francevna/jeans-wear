@extends('layouts.app')

@section('title', 'Новости')

@section('content')
    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">Новости</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pages-title-end -->
    <!-- Blog content section start -->
    <section class="blog-area section-padding-bottom">
        <div class="container">
            <div class="row">
                <div>
                    @foreach($news as $item)
                    <div class="tb-post-item">
                        <a href="{{route('post', $item->slug)}}">
                            <div class="tb-thumb">
                                <img src="{{asset($item->image)}}" alt="">
                                <span class="tb-publish font-noraure-3">{{ \Carbon\Carbon::parse($item->created_at)->format('d.m.y')}}</span>
                            </div>
                        </a>
                        <div class="tb-content7">
                            <a href="{{route('post', $item->slug)}}"><h4 class="tb-titlel">{{$item->title}}</h4></a>
                            <div class="tb-excerpt">{!! str_limit($item->text, $limit = 300, $end = '...') !!}
                            </div>
                            <a class="blog7" href="{{route('post', $item->slug)}}">Подробнее</a>
                        </div>
                    </div>
                    @endforeach

                    @if ($news->lastPage() > 1)
                        <nav class="pagination tb-pagination text-right">
                            @for ($i = 1; $i <= $news->lastPage(); $i++)
                                <a class="page-numbers{{ ($news->currentPage() == $i) ? ' current' : '' }}" href="{{ $news->url($i) }}">
                                   {{ $i }}
                                </a>
                            @endfor
                            <a class="next page-numbers {{ ($news->currentPage() == $news->lastPage()) ? ' disabled' : '' }}" href="{{ $news->url($news->currentPage()+1) }}"><i class="fa fa-angle-right"></i></a>
                        </nav>
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection