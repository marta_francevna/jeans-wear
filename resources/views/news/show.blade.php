@extends('layouts.app')

@section('title', $news->title)

@section('content')

    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">{{$news->title}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pages-title-end -->
    <!-- Blog content section start -->
    <section class="blog-area bd-area">
        <div class="container">
            <div class="row">
                <div>
                    <div class="tb-post-item ma-nn">
                        <a href="#">
                            <div class="tb-thumb">
                                <img src="{{asset($news->image)}}" alt="">
                                <span class="tb-publish font-noraure-3">{{ \Carbon\Carbon::parse($news->created_at)->format('d.m.y')}}</span>
                            </div>
                        </a>
                        <div class="tb-content7">
                            <h4 class="tb-titlel">{{$news->title}}</h4>
                            <div class="blog-desc">
                                <p>{!! $news->text !!}</p>
                            </div>
                            <div class="next-pre ">
                                @if(isset($previous))
                                    <a class="blog1" href="{{route('post', $previous->slug)}}">
                                        <i class="fa fa-angle-left"></i> Прошлая</a>
                                @endif
                                @if(isset($next))
                                    <a class="blog2" href="{{route('post', $next->slug)}}"> Следующая
                                        <i class="fa fa-angle-right"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection