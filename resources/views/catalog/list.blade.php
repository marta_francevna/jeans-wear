<div class="col-md-8 col-lg-9 col-sm-12 section-padding-bottom">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="features-tab">

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="row">
                            <!-- Nav tabs -->
                            <div class="shop-tab">
                            @if (isset($catalogs) && count($catalogs)>0)
                                <!-- single-product start -->
                                    @foreach($catalogs as $item)
                                        <div class="col-md-4 col-lg-4 col-sm-6">
                                            @if($item->id == 1 || $item->id == 2 || $item->id == 3)
                                                <div class="single-product">
                                                    @else
                                                        <div class="single-product s-top s-non">
                                                            @endif
                                                            <div class="product-img">
                                                                <a href="{{route('catalog', [$item->category->sex, $item->category->slug, $item->id])}}">
                                                                    <img src="{{asset($item->image)}}" alt="{{$item->category->name}}e"/>
                                                                    <img class="secondary-image" alt="{{$item->category->name}}" src="{{asset($item->image)}}">
                                                                </a>
                                                            </div>
                                                            <div class="product-dsc">
                                                                <h3>
                                                                    <a href="{{route('catalog', [$item->category->sex, $item->category->slug, $item->id])}}">
                                                                        {{ str_limit($item->category->name . ' / ' . $item->vendor_code, $limit = 30, $end = '') }}
                                                                    </a></h3>
                                                            </div>
                                                        </div>
                                                </div>
                                                @endforeach
                                            @else
                                                <p>Извините, ничего не найдено :(</p>
                                                <!-- single-product end -->
                                            @endif
                                        </div>
                            </div>
                        </div>

                    </div>
                    @if (isset($catalogs) && count($catalogs)>0)
                        @if ($catalogs->lastPage() > 1)
                            <div class="shop-all-tab-cr shop-bottom">
                                <div class="two-part">
                                    <div class="shop5 page">
                                        <ul>
                                            <li>
                                                @for ($i = 1; $i <= $catalogs->lastPage(); $i++)
                                                    <a class="{{ ($catalogs->currentPage() == $i) ? ' active' : '' }}" href="{{ $catalogs->url($i) }}">
                                                        {{ $i }}
                                                    </a>
                                                @endfor
                                                <a class="{{ ($catalogs->currentPage() == $catalogs->lastPage()) ? ' disabled' : '' }}" href="{{ $catalogs->url($catalogs->currentPage()+1) }}"><i class="fa fa-arrow-right"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>