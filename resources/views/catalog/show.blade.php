@extends('layouts.app')

@section('title', $catalog->category->name)

@section('content')
    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">{{$catalog->category->name}} / {{$catalog->vendor_code}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="single-product-area sit section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-9" style="width: 100%;">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 none-si-pro" style="width: 25%">
                            <div class="pro-img-tab-content tab-content">
                                <div class="tab-pane active" id="image-1">
                                    <div class="simpleLens-big-image-container">
                                        <a class="simpleLens-lens-image" data-lightbox="roadtrip" data-lens-image="{{asset($catalog->image)}}" href="{{asset($catalog->image)}}">
                                            <img src="{{asset($catalog->image)}}" alt="" class="simpleLens-big-image">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9" style="margin-top: -60px">
                            <div class="text">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Описание</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Комментарии</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content tab-con2">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <p class="availability in-stock">
                                            <b>Артикул: </b>{{$catalog->vendor_code}}
                                        </p>
                                        <p class="availability in-stock2">
                                            <b>Состав:</b> {{$catalog->composition}}
                                        </p>
                                        <p class="availability in-stock2">
                                            <b> Цвета:</b>
                                            @foreach($catalog->colors as $color)
                                                {{ $color->name }}
                                            @endforeach
                                        </p>
                                        @if ($catalog->category->sex !== 'unisex')
                                            <p class="availability in-stock2">
                                                <b> Размеры:</b>
                                                @foreach($catalog->size as $size)
                                                    {{ $size->name }}
                                                @endforeach
                                            </p>
                                        @endif
                                        <div class="pre-box">
                                            <span class="special-price" style="color: rgb(239, 102, 68); font-family: aleo-regular; font-weight: 600; font-size: 30px; display: block; margin-top: 20px">{{$catalog->price}} BYN</span>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                            @if(isset($comments))
                                                <div id="review">
                                                    @foreach($comments as $comment)
                                                        <table class="table table-striped table-bordered">
                                                            <tr>
                                                                <td class="text-right">{{ \Carbon\Carbon::parse($comment->created_at)->format('d/m/y')}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <p class="text an-text">{{$comment->message}}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    @endforeach
                                                    <div class="text-right"></div>
                                                </div>
                                            @endif
                                                <h2 class="write"></h2>
                                                <div class="form-group required">
                                                <div class="col-sm-12">
                                                    <form method="post" action="{{ route('catalog.comment', [$catalog->id], false) }}">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                        <textarea id="input-review" required class="form-control" rows="2" style="resize: none" placeholder="Напишите комментарий.." name="message"></textarea>
                                                        <div class="buttons si-button">
                                                            <button id="button-review" class="btn btn-primary"  type="submit">
                                                                Отправить
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection