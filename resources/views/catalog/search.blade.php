@extends('layouts.app')

@section('title', 'Поиск')

@section('content')

    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        @if(!empty($title['category'])||!empty($title['color'])||!empty($title['size']))
                            <h2 class="page-title">Ваш запрос:
                                {{!empty($title['category'])?$title['category']->name:''}}
                                {{!empty($title['color'])?$title['color']->name:''}}
                                {{!empty($title['size'])?$title['size']->name:''}}
                            </h2>
                        @else
                            <h2 class="page-title">Каталог </h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pages-title-end -->
    <!-- shop-style content section start -->
    <section class="pages products-page section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3 col-sm-12">
                    <div class="all-shop-sidebar">
                        <div class="top-shop-sidebar">
                            <h3 class="wg-title">Поиск</h3>
                        </div>
                        <form class="cendo" method="GET" action="{{ route('catalogs.search', [], false) }}">
                            <div class="shop-one">
                                <h3 class="wg-title2">Категория</h3>
                                <div class="shop6">
                                    <select name="category" id="category">
                                        <option value=''>Выберите</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="shop-one">
                                <h3 class="wg-title2">Цвет</h3>
                                <div class="shop6">
                                    <select name="color" id="colors">
                                        <option value=''>Выберите</option>
                                        @foreach($colors as $color)
                                            <option value="{{$color->id}}">{{$color->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="shop-one" id="size">
                                <h3 class="wg-title2">Размер</h3>
                                <div class="shop6">
                                    <select name="size" id="sizes">
                                        <option value=''>Выберите</option>
                                        @foreach($sizes as $size)
                                            <option value="{{$size->id}}">{{$size->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-search" type="submit"><i class="fa fa-search fa-fw"></i> Найти
                                </button>

                            </div>

                        </form>
                    </div>
                </div>
                @include('catalog.list')
            </div>
        </div>
    </section>
    <!-- shop-style  content section end -->
    <!-- quick view start -->

@endsection