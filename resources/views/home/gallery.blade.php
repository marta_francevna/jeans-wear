@extends('layouts.app')

@section('title', 'Фотогаллерея')

@section('content')

    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">Фотогаллерея</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main_shop_area">
        <div class="breadcrumbs">
            <div class="container">
                <div class="row creative-member-area">
                    <div class="about-sec-head">
                        <h2 class="creative-member">
                            Галлерея
                            <strong>Джинсовой одежды</strong>
                        </h2>
                        <p>Здесь вы можете видеть фотографии джинсовой одежды и посмотреть более детально понравившуюся вам вещь ♡</p>
                    </div>
                    @foreach($catalogs as $catalog)
                        <div class="col-md-3 col-sm-6">
                            <div class="single-creative-member res2">
                                <div class="member-image">
                                    <a href="{{route('catalog', [$catalog->category->sex, $catalog->category->slug, $catalog->id])}}">
                                        <img alt="" src="{{asset($catalog->image)}}">
                                    </a>
                                    <div class="member-title">
                                        @if($catalog->category->sex == 'men')
                                            <h2>Мужская одежда</h2>
                                        @elseif($catalog->category->sex == 'woman')
                                            <h2>Женская одежда</h2>
                                        @else
                                            <h2>Головной убор</h2>
                                        @endif
                                        <h3>{{$catalog->category->name}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection