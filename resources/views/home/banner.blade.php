<div class="banner-area">
    <div class="container">
        <div class="section-padding1">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="banner-box banner-box-re">
                        <a href="{{route('category',['woman','jeans'])}}">
                            <img alt="" src="{{asset('img/banner/1.png')}}">
                            <div>
                                <h2>
                                    Женские джинсы
                                </h2>
                                <p>Всегда женственно, элегантно и сексуально</p>
                            </div>
                        </a>
                    </div>
                    <div class="banner-box res-btm">
                        <a href="{{route('category', ['men','jeans'])}}">
                            <img alt="" src="{{asset('img/banner/5.png')}}">
                            <div>
                                <h2>
                                    Мужские джинсы
                                </h2>
                                <p>Классика, которая никогда не выйдет из моды</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="banner-container banner-box res-btm">
                        <a href="{{route('category', ['woman','overalls'])}}">
                            <img alt="" src="{{asset('img/banner/3.png')}}">
                            <div>
                                <h2>
                                    Комбинезон
                                </h2>
                                <p>Удобная и красивая вещь, которая должна быть в шкафу у каждой девушки</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="banner-box banner-box-re">
                        <a href="{{route('category', ['men','jacket'])}}">
                            <img alt="" src="{{asset('img/banner/4.png')}}">
                            <div>
                                <h2>
                                    Джинсовка
                                </h2>
                                <p>Вещь, без которой не обойтись ни весной, ни осенью</p>
                            </div>
                        </a>
                    </div>
                    <div class="banner-box">
                        <a href="{{route('category',['unisex','panama'] )}}">
                            <img alt="" src="{{asset('img/banner/2.png')}}">
                            <div>
                                <h2>
                                    Панама
                                </h2>
                                <p>Неотъемлемый летний аксессуар</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>