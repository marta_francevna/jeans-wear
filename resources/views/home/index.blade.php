@extends('layouts.app')

@section('title', 'JeansWear')

@section('content')
    @include('home.slider')
    @include('home.banner')
    <section class="featured-products single-products section-padding-top">

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title">
                        <h3>Каталог</h3>
                        <div class="section-icon">
                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="product-tab nav nav-tabs">
                        <ul>
                            <li class="active"><a data-toggle="tab" href="#all">Все</a></li>
                            <li><a data-toggle="tab" href="#woman">Женское</a></li>
                            <li><a data-toggle="tab" href="#man">Мужское</a></li>
                            <li><a data-toggle="tab" href="#unisex">Головные уборы</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row tab-content">
                <div class="tab-pane  fade in active" id="all">
                    <div id="tab-carousel-1" class="re-owl-carousel owl-carousel product-slider owl-theme">
                        @for ($i = 0; $i < count($catalog); $i++)
                            <div class="col-xs-12">
                                <div class="single-product">
                                    <div class="product-img">
                                        <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                            <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                            <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                        </a>
                                    </div>
                                    <div class="product-dsc">
                                        <h3>
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                        </h3>
                                    </div>
                                </div>
                                @if($i!=(count($catalog)-1))
                                    @php($i++)
                                    <div class="single-product margin-top">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                @endif
                            </div>
                    @endfor
                    <!-- single product end -->
                    </div>
                </div>

                <div class="tab-pane  fade in" id="woman">
                    <div id="tab-carousel-2" class="owl-carousel product-slider owl-theme">
                        @for ($i = 0; $i < count($catalog); $i++)
                            @if($catalog[$i]['category']['sex']=='woman')
                                <div class="col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                    @if($i!=(count($catalog)-1))
                                        @php($i++)
                                        <div class="single-product margin-top">
                                            <div class="product-img">
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                    <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                    <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                                </a>
                                            </div>
                                            <div class="product-dsc">
                                                <h3>
                                                    <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                                </h3>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                        @endif
                    @endfor
                    <!-- single product end -->
                    </div>
                </div>

                <div class="tab-pane  fade in" id="man">
                    <div id="tab-carousel-3" class="owl-carousel product-slider owl-theme">
                        @for ($i = 0; $i < count($catalog); $i++)
                            @if($catalog[$i]['category']['sex']=='men')
                                <div class="col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                    @php($i++)
                                    <div class="single-product margin-top">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    @endfor
                    <!-- single product end -->
                    </div>
                </div>

                <div class="tab-pane  fade in" id="unisex">
                    <div id="tab-carousel-4" class="owl-carousel product-slider owl-theme">
                        @for ($i = 0; $i < count($catalog); $i++)
                            @if($catalog[$i]['category']['sex']=='unisex')
                                <div class="col-xs-12">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                    @php($i++)
                                    <div class="single-product margin-top">
                                        <div class="product-img">
                                            <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">
                                                <img src="{{asset($catalog[$i]['image'])}}" alt="{{$catalog[$i]['category']['name']}}"/>
                                                <img class="secondary-image" alt="{{$catalog[$i]['category']['name']}}" src="{{asset($catalog[$i]['image'])}}">
                                            </a>
                                        </div>
                                        <div class="product-dsc">
                                            <h3>
                                                <a href="{{route('catalog', [$catalog[$i]['category']['sex'], $catalog[$i]['category']['slug'], $catalog[$i]['id']])}}">{{ str_limit($catalog[$i]['category']['name'] . ' / ' . $catalog[$i]['vendor_code'], $limit = 30, $end = '') }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                        @endif
                    @endfor
                    <!-- single product end -->
                    </div>
                </div>
                <!-- accessories product end -->
            </div>
        </div>
    </section>
    @include('home.last')
    @include('home.news')
@endsection