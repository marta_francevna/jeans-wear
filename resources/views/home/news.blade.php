<section class="blog section-padding-top section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>Новости</h3>
                    <div class="section-icon">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="blog" class="owl-carousel product-slider owl-theme re-blog">
                @foreach( $news as $item)
                <div class="col-xs-12">
                    <div class="blog-container-inner">
                        <div class="post-thumb">
                            <a href="{{route('post', $item->slug)}}"><img class="attachment-blog-list wp-post-image" alt="blog-2" src="{{asset($item->image)}}"></a>
                        </div>
                        <div class="visual-inner">
                            <h2 class="blog-title">
                                <a href="{{route('post', $item->slug)}}"> {{$item->title}}</a>
                            </h2>
                            <div class="blog-content">{!! str_limit($item->text, $limit = 150, $end = '...')  !!}
                                </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- single blog item end -->
            </div>
        </div>
    </div>
</section>