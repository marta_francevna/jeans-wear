<section class="slider-main-area">
    <div class="main-slider an-si">
        <div class="bend niceties preview-2 hm-ver-1">
            <div id="ensign-nivoslider-2" class="slides">
                <img src="{{asset('img/slider/5.png')}}" alt="" title="#slider-direction-1"  />
                <img src="{{asset('img/slider/2.png')}}" alt="" title="#slider-direction-2"  />
                <img src="{{asset('img/slider/6.jpg')}}" alt="" title="#slider-direction-3"  />
            </div>
            <!-- direction 1 -->
            <div id="slider-direction-1" class="t-cn slider-direction Builder">
                <div class="slide-all">

                    <!-- layer 2 -->
                    <div class="layer-2">
                        <h2 class="title6">Джинсовая одежда</h2>
                    </div>
                    <!-- layer 2 -->
                    <div class="layer-2">
                        <p class="title0">Must have</p>
                    </div>
                    <!-- layer 3 -->
                    <div class="layer-3">
                        <a class="min1" href="{{route('catalogs')}}">Показать</a>
                    </div>
                </div>
            </div>
            <div id="slider-direction-2" class="t-cn slider-direction Builder">
                <div class="slide-all slide2">
                    <!-- layer 2 -->
                    <div class="layer-2">
                        <h2 class="title6">Мужская одежда</h2>
                    </div>
                    <!-- layer 3 -->
                    <div class="layer-3">
                        <a class="min1" href="{{route('catalog.sex', ['men'])}}">Показать</a>
                    </div>
                </div>
            </div>
            <div id="slider-direction-3" class="t-cn slider-direction Builder">
                <div class="slide-all slide2">
                    <!-- layer 2 -->
                    <div class="layer-2">
                        <h2 class="title6">Женская одежда</h2>
                    </div>
                    <!-- layer 3 -->
                    <div class="layer-3">
                        <a class="min1" href="{{route('catalog.sex', ['woman'])}}">Показать</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>