<section class="new-products single-products section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title">
                    <h3>Последнее</h3>
                    <div class="section-icon">
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="new-products" class="owl-carousel product-slider owl-theme">
               @foreach($latestCatalogs as $item)
                <div class="col-xs-12">
                    <div class="single-product">
                        <div class="product-img">
                            <a href="{{route('catalog', [$item->category->sex, $item->category->slug, $item->id])}}">
                                <img src="{{asset($item->image)}}" alt="{{$item->category->name}}" />
                                <img class="secondary-image" alt="{{$item->category->name}}" src="{{asset($item->image)}}">
                            </a>
                        </div>
                        <div class="product-dsc">
                            <h3><a href="{{route('catalog', [$item->category->sex, $item->category->slug, $item->id])}}">{{$item->category->name}} / {{$item->vendor_code}}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- single product end -->
            </div>
        </div>
    </div>
</section>