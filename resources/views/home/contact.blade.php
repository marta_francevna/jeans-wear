@extends('layouts.app')

@section('title', 'Контакты')

@section('content')

    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">Контакты</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pages-title-end -->
    <!-- contact content section start -->
    <section class="top-map-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="page-title">
                        <h3>Контактные данные</h3>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <strong>Почта</strong>
                                    <br>
                                    <address>ilushaburic@gmail.com</address>
                                </div>
                                <div class="col-sm-3">
                                    <strong>Телефон</strong>
                                    <br>
                                    +375 (29) 212-47-37
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Обратная связь</h3>
                        </div>
                        <form class="cendo" action="{{ route('contact.feedback', [], false) }}" method="post">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="form-group required">
                                <label class="col-md-2 control-label">Имя</label>
                                <div class="col-md-10">
                                    <input class="form-control" required type="text" name="name">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-2 control-label">E-Mail</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="email" required name="email">
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-2 control-label">Сообщение</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="10" required name="message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="buttons">
                                    <div class="pull-right">
                                        <input class="btn btn-primary" type="submit" value="Отправить">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact content section end -->

@endsection