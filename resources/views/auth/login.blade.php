@extends('layouts.app')

@section('content')

    <section class="contact-img-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="con-text">
                        <h2 class="page-title">Вход</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- pages-title-end -->
    <!-- login content section start -->
    <div class="login-area section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="tb-login-form ">
                        <h5 class="tb-title">Вход</h5>
                        <form role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <p class="checkout-coupon top log a-an">
                                <label class="l-contact">
                                    Email
                                    <em>*</em>
                                </label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
          </span>
                                @endif
                            </p>
                            <p class="checkout-coupon top-down log a-an">
                                <label class="l-contact">
                                    Пароль
                                    <em>*</em>
                                </label>
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
          </span>
                                @endif
                            </p>

                            <p class="login-submit5">
                                <input class="button-primary" type="submit" value="Войти">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
