<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset("img/favicon.ico")}}">
    <!-- google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>
    <!-- all css here -->
    <!-- bootstrap v3.3.6 css -->
    <link rel="stylesheet" href="{{asset("css/bootstrap.min.css")}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{asset("css/animate.css")}}">
    <!-- pe-icon-7-stroke -->
    <link rel="stylesheet" href="{{asset("css/pe-icon-7-stroke.min.css")}}">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="{{asset("css/jquery-ui.min.css")}}">
    <!-- Image Zoom CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset("css/img-zoom/jquery.simpleLens.css")}}">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{asset("css/meanmenu.min.css")}}">
    <!-- nivo slider CSS
    ============================================ -->
    <link rel="stylesheet" href="{{asset("lib/css/nivo-slider.css")}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset("lib/css/preview.css")}}" type="text/css" media="screen"/>
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{asset("css/owl.carousel.css")}}">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{asset("css/font-awesome.min.css")}}">
    <!-- style css -->
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{asset("css/responsive.css")}}">
    <!-- modernizr css -->
    <script src="{{asset("js/vendor/modernizr-2.8.3.min.js")}}"></script>
    <style>

        .single-menu span a:hover {
            color: black !important;
        }

        .single-menu span a {
            color: #333 !important;
            font-size: 14px !important;
            text-transform: none !important;
            padding: 0 0 !important;
            margin: 3px 0 !important;
        }

    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header section start -->
<header>
    <div class="header-bottom header-bottom-one" id="sticky-menu">
        <div class="container relative">
            <div class="row">
                <div class="col-sm-2 col-md-2 col-xs-4">
                    <div class="logo">
                        <a href="/"><img src="{{asset('img/logo.png')}}" alt=""/></a>
                    </div>
                </div>
                <div class="col-sm-10 col-md-10 col-xs-8 static">
                    <div class="all-manu-area">
                        <div class="mainmenu clearfix hidden-sm hidden-xs">
                            <nav>
                                <ul>
                                    <li><a href="{{route('catalogs')}}">Каталог</a>
                                        <div class="magamenu ">
                                            <ul class="again">
                                                <li class="single-menu colmd4">
                                                    <span><a href="{{route('catalog.sex', ['men'])}}">Мужская одежда</a> </span>
                                                    <a href="{{route('category',['men','jacket'] )}}">Куртки</a>
                                                    <a href="{{route('category',['men','jeans'] )}}">Джинсы</a>
                                                    <a href="{{route('category',['men','shorts'] )}}">Шорты</a>
                                                    <a href="{{route('category',['men','costume'] )}}">Костюмы</a>
                                                </li>
                                                <li class="single-menu colmd4">
                                                    <span><a href="{{route('catalog.sex', ['woman'])}}">Женская одежда</a></span>
                                                    <a href="{{route('category',['woman','jacket'] )}}">Куртки</a>
                                                    <a href="{{route('category',['woman','jeans'] )}}">Джинсы</a>
                                                    <a href="{{route('category',['woman','shorts'] )}}">Шорты</a>
                                                    <a href="{{route('category',['woman','overalls'] )}}">Комбинезоны</a>
                                                </li>
                                                <li class="single-menu colmd4">
                                                    <span><a href="{{route('catalog.sex', ['unisex'])}}">Головные уборы</a></span>
                                                    <a href="{{route('category',['unisex','cap'] )}}">Кепки</a>
                                                    <a href="{{route('category',['unisex','panama'] )}}">Панамы</a>
                                                </li>

                                                <li class="single-menu colmd4 colmd5">
                                                    <a href="{{route('catalogs')}}">
                                                        <img alt="" src='{{asset("img/maga-banner.png")}}'>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="{{route('gallery')}}">Фотогаллерея</a></li>
                                    <li><a href="{{route('news')}}">Новости</a></li>
                                    <li><a href="{{route('contact')}}">Контакты</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!-- mobile menu start -->
                        <div class="mobile-menu-area hidden-lg hidden-md">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li><a href="{{route('catalogs')}}">Каталог</a>
                                            <ul>
                                                <li>
                                                    <span><a href="{{route('catalog.sex', ['men'])}}">Мужская одежда</a> </span>
                                                </li>
                                                <li>
                                                    <span><a href="{{route('catalog.sex', ['woman'])}}">Женская одежда</a></span>
                                                </li>
                                                <li>
                                                    <span><a href="{{route('catalog.sex', ['unisex'])}}">Головные уборы</a></span>
                                                </li>

                                            </ul>
                                        </li>
                                        <li><a href="{{route('gallery')}}">Фотогаллерея</a></li>
                                        <li><a href="{{route('news')}}">Новости</a></li>
                                        <li><a href="{{route('contact')}}">Контакты</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- mobile menu end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header section end -->
<div id="app" class='content'>
    @yield('content')
</div>
<!-- footer section start -->
<footer class="re-footer-section">
    <!-- footer-top area start -->
    <div class="footer-top section-padding-top ">
        <div class="footer-dsc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="single-text res-text">
                            <div class="footer-title">
                                <h4>Контакты</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-text">
                                <ul>
                                    <li>
                                        <i class="pe-7s-call"></i>
                                        <p>+375 (29) 212-47-37</p>
                                    </li>
                                    <li>
                                        <i class="pe-7s-mail-open"></i>
                                        <p>ilushaburic@gmail.com</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="single-text res-text">
                            <div class="footer-title">
                                <h4>Мои аккаунты</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-menu">
                                <ul>
                                    <li><a href="https://vk.com/obida_ebanaya">Вконтакте</a></li>
                                    <li><a href="http://instagram.com/ilushkaburic">Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 hidden-sm col-md-4 margin-top">
                        <div class="single-text res-text">
                            <div class="footer-title">
                                <h4>Обратная связь</h4>
                                <hr class="dubble-border"/>
                            </div>
                            <div class="footer-text">
                                <form action="{{ route('feedback', [], false) }}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    <textarea required name="message" rows="4"
                                              placeholder="Введите сообщение..."></textarea>
                                    <input type="submit" value="Отправить"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="dubble-border"/>
        </div>
    </div>
    <!-- footer-top area end -->
    <!-- footer-bottom area start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>&copy; 2018г. Илья Бураков</p>
                </div>
            </div>
            <div class="row">
                <div class="">
                    @if(!Auth::check())
                        <div class="right-header">
                            <ul>
                                <li><a href="{{route('login')}}" class="active"><i class="fa fa-sign-in"></i>Войти</a>
                                </li>
                            </ul>
                        </div>
                    @else
                        <div class="right-header">
                            <ul>
                                <li><a href="{{route('logout')}}" class="active" onclick="event.preventDefault();
                                document.getElementById('logout').submit();"><i class="fa fa-sign-out"></i>Выйти</a>
                                </li>

                                <form id="logout" action="{{ route('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                </form>

                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom area end -->
</footer>
<!-- footer section end -->

<!-- all js here -->
<script src="{{ asset('js/app.js') }}"></script>
<!-- jquery latest version -->
<script src="{{asset("js/vendor/jquery-1.12.0.min.js")}}"></script>

<!-- bootstrap js -->
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<!-- parallax js -->
<script src="{{asset("js/parallax.min.js")}}"></script>
<!-- owl.carousel js -->
<script src="{{asset("js/owl.carousel.min.js")}}"></script>
<!-- Img Zoom js -->
<script src="{{asset("js/img-zoom/jquery.simpleLens.min.js")}}"></script>
<!-- meanmenu js -->
<script src="{{asset("js/jquery.meanmenu.js")}}"></script>
<!-- jquery.countdown js -->
<script src="{{asset("js/jquery.countdown.min.js")}}"></script>
<!-- Nivo slider js
============================================ -->
<script src="{{asset("lib/js/jquery.nivo.slider.js")}}" type="text/javascript"></script>
<script src="{{asset("lib/home.js")}}" type="text/javascript"></script>
<!-- jquery-ui js -->
<script src="{{asset("js/jquery-ui.min.js")}}"></script>
<!-- sticky js -->
<script src="{{asset("js/sticky.js")}}"></script>
<!-- plugins js -->
<script src="{{asset("js/plugins.js")}}"></script>
<!-- main js -->
<script src="{{asset("js/main.js")}}"></script>
</body>
</html>
